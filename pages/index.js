import Nav from '../components/nav'

export default function IndexPage() {
  return (
    <div>
      <Nav />
      <div className="p-20">
        <h1 className="text-2xl font-bold">
          🍌 bananabred
        </h1>
        <p className="p-20 font-normal text-base">
        Banana Bread
    •     2 xícaras (250g) de farinha multiuso (colher e nivelada)
    •     1 colher de chá de bicarbonato de sódio
    •     1/4 colher de chá de sal
    •     1/2 colher de chá de canela em pó
    •     1/2 xícara (1 palito ou 115g) de manteiga sem sal, amolecida à temperatura ambiente
    •     3/4 xícara (150g) de açúcar mascavo claro ou escuro embalado
    •     2 ovos grandes, em temperatura ambiente
    •     1/3 xícara (80g) de iogurte natural ou creme de leite (eu uso iogurte grego)
    •     2 xícaras de banana amassada (cerca de 4 bananas maduras grandes)
    •     1 colher de chá de extrato de baunilha puro
    •     opcional: 3/4 xícara (100g) de nozes ou nozes picadas
Instruções
Ajuste a grelha do forno para a terceira posição inferior e pré-aqueça o forno a 180 ° C. Unte uma forma de pão. Reserve.
Bata a farinha, o bicarbonato, o sal e a canela em uma tigela grande.
Usando uma batedeira ou batedeira equipada com uma pá ou batedeira, bata a manteiga e o açúcar mascavo em alta velocidade até ficar homogêneo e cremoso, cerca de 2 minutos. Em velocidade média, acrescente os ovos um a um, batendo bem a cada adição. Junte o iogurte, a banana amassada e o extrato de baunilha em velocidade média até incorporar.  Com a batedeira funcionando em velocidade baixa, bata lentamente os ingredientes secos nos úmidos até que não haja mais nenhuma camada de farinha. Não misture demais.
Coloque a massa na assadeira preparada e leve ao forno por 60-65 minutos.  Cubra levemente o pão com papel alumínio após 30 minutos para ajudar a evitar que a parte superior e as laterais fiquem muito douradas.  Um palito inserido no centro do pão sairá limpo quando o pão estiver pronto.  Retire do forno e deixe o pão esfriar completamente na forma sobre uma gradinha.
Cubra e guarde o pão de banana em temperatura ambiente por 2 dias ou na geladeira por até 1 semana.  O pão de banana fica mais saboroso no segundo dia, depois que os sabores se estabilizam.
        </p>
      </div>
    </div>
  )
}
